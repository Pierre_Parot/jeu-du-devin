import random

def jeu_devin():

    ''' R0 : Faire deviner un nombre compris entre 0 et 9999 à l'utilisateur
        R1 : Comment lui faire deviner ce nombre?
             -Choisir un nombre
             -Demander son choix à l'utilisateur
             -Evaluer la réponse et indiquer à l'utilisateur si elle est inférieure ou supérieure
              au nombre recherché jusqu'à résolution.
        R2 : Evaluer la réponse et indiquer à l'utilisateur si elle est inférieure ou supérieure
             au nombre recherché jusqu'à résolution.
             - Comparer la réponse au nombre recherché
             - Donner une indication à l'utilisateur : inférieur ou supérieur
             - Recommencer jusqu'à résolution
             - Afficher le nombre de coups
    '''
             
    
    nb = random.randint(1, 9999) # Générer le nombre aléatoire
    print('J\'ai choisi un nombre entre 1 et 9999')
    rep = int(input("Proposition 1 : ")) # Poser une première question à l'utilisateur
        
    cpt = 1 # Initialiser le compteur de coups
    
    ''' Evaluer la réponse de l'utilisateur jusqu'à résolution '''
    while rep != nb: # Répéter jusqu'à résolution
        cpt += 1 # Actualiser le nombre de coups
        if rep > nb : # Comparer les valeurs 
            print('Trop grand') # Donner l'indication "trop grand" à l'utilisateur
        else:
            print('Trop petit') # Donner l'indication "trop petit" à l'utilisateur
        rep = int(input(f'Proposition {cpt} : ')) # Récupérer la nouvelle réponse de l'utilisateur   
        
    print(f'Bravo! Vous avez trouvé en {cpt} coups.') # Afficher le nombre de coups.
    



def jeu_devin_machine():
    
    ''' R0 : Faire deviner un nombre, compris entre 1 et 9999, choisi par l'utilisateur, à la machine.
        R1 : Comment lui faire deviner ce nombre?
             - Proposer une première estimation : médiane
             - Récupérer l'indication fournie par l'utilisateur : inférieur ou supérieur
             - Recommencer jusqu'à résolution
        R2 : Recommencer jusqu'à résolution : Procéder par dichotomie
             - Calculer la médiane entre les deux bornes
             - Récupérer l'indication de l'utilisateur
             - Calculer la nouvelle médiane entre notre réponse et la borne haute si trop petit ou la borne basse si trop haut
             
    '''
    print("Choisissez un nombre entre 1 et 9999")
    
    borne_b, borne_h = 1, 9999 # Initialisation des bornes
    reponse = (borne_b + borne_h)//2 # Calcul de la médiane
    indic = input(f'{reponse} est-il trop petit (p,P,<), trop grand (g,G,>), ou trouvé (t, T, =)?') # Récupération de l'indice
    while indic not in ('p', 'g', 't', 'P', 'G', 'T', '<', '>', '='):  # Redemander l'indice en cas de mauvaise réponse
        indic = input(f'Je n\'ai pas compris votre indication \nRépondez (p,P,<) si {reponse} est trop petit, (g,G,>) si trop grand ou (t,T,=) si j\'ai trouvé.')

    
    cpt = 1  # Initialisation du compteur de coups
    '''Recommencer jusqu'à résolution'''
    while indic not in ('t', 'T', '='): # Répéter tant que l'on n'a pas trouvé 
        if indic in ('p', 'P', '<'): # Calcul de la nouvelle médiane entre la proposition et la borne haute si trop petit
            borne_b = reponse
            reponse = (borne_h + reponse) // 2
        else : # Calcul de la nouvelle médiane entre la proposition et la borne basse si trop haut
            borne_h = reponse
            reponse = (borne_b + reponse) // 2
        indic = input(f'{reponse} est-il trop petit (p), trop grand (g), ou trouvé (t)?') # Récupération du nouvel indice
        while indic not in ('p', 'g', 't', 'P', 'G', 'T', '<', '>', '='): # Redemander l'indice en cas de mauvaise réponse
            indic = input(f'Je n\'ai pas compris votre indication, répondez (p,P,<) si {reponse} est trop petit, (g,G,>) si trop grand ou (t,T,=) si j\'ai trouvé.')
        cpt += 1 # Incrémentation du compteur de coups
        
    print(f'La réponse était donc {reponse}! J\'ai trouvé en {cpt} coups!') # Affichage du résultat et du nombre de coups
    
    
choix = input(f'1- L\'ordinateur choisit un nombre et vous le devinez \n2- Vous choissez un nombre et l\'ordinateur le devine \n0- Quitter le programme \n') # Choix du mode de jeu
while choix not in ('0', '1', '2'): # Redemander en cas de mauvaise réponse
    print('Je n\'ai pas compris')
    choix = input(f'1- L\'ordinateur choisit un nombre et vous le devinez \n2- Vous choissez un nombre et l\'ordinateur le devine \n0- Quitter le programme \n')
print(f'Votre choix : {choix}')


'''Lancer le programme correspondant au choix ou quitter'''
if choix == '1': 
    jeu_devin()
elif choix == '2':
    jeu_devin_machine()
else:
    print('Au revoir!')



    
    
         
    
